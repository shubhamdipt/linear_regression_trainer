from sklearn import linear_model
import pandas as pd


class Predictor:

    def __init__(self,
                 training_data,
                 independent_variables,
                 dependent_variable,
                 test_data,
                 ):
        self.training_data = training_data
        self.test_data = test_data
        self.independent_variables = independent_variables
        self.dependent_variable = dependent_variable
        self.linear_model = None
        self.train_preprocessed = None
        self.test_preprocessed = None
        self.predictions = None

    def create_city_dummy_variables(self):
        """Creates dummy variables"""
        train_objs_num = self.training_data.shape[0]
        train = self.training_data[self.independent_variables]
        test = self.test_data[self.independent_variables]
        dataset = pd.concat(objs=[train, test], axis=0)
        dataset_preprocessed = pd.get_dummies(dataset)
        self.train_preprocessed = dataset_preprocessed[:train_objs_num]
        self.test_preprocessed = dataset_preprocessed[train_objs_num:]

    def develop_model(self):
        """Creates a linear regression model based on training data."""
        x_data = self.train_preprocessed
        y_data = self.training_data[self.dependent_variable]
        lm = linear_model.LinearRegression()
        lm.fit(x_data, y_data)
        # The coefficients
        print('Training Model ...')
        print('Coefficients: \n', lm.coef_)
        # The intercept
        print('Intercept: \n', lm.intercept_)
        # The mean squared error
        print('Variance score: %.2f' % lm.score(x_data, y_data))
        self.linear_model = lm

    def test_predictions(self):
        """Testing predictions using test data."""
        x_data = self.test_preprocessed
        self.predictions = pd.DataFrame(self.linear_model.predict(x_data))

    def export_to_csv(self, filename):
        """Exports the data to CSV."""
        self.test_data[self.dependent_variable] = self.predictions
        self.test_data.to_csv(filename)