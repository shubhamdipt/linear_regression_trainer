import numpy as np
import pandas as pd
from trainer import Predictor
import warnings
warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")


def main():
    training_data = pd.read_csv('training.csv')
    test_data = pd.read_csv('test.csv')
    predictor = Predictor(
            training_data,
            ['job_type', 'hours', 'city', 'salary'],
            ['applications'],
            test_data
    )
    predictor.create_city_dummy_variables()
    predictor.develop_model()
    predictor.test_predictions()
    predictor.export_to_csv('results.csv')

if __name__ == "__main__":
    main()